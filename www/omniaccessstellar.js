/*global cordova, module*/

module.exports = {
    init: function (name, successCallback, errorCallback) {
        console.log("lbs - init : " + name);
        cordova.exec(successCallback, errorCallback, "OmniaccessStellar", "init", [name]);
    },
    location: function (data, successCallback, errorCallback) {
        console.log("lbs - location : " + data);
        cordova.exec(successCallback, errorCallback, "OmniaccessStellar", "location", [data]);
    }
};
