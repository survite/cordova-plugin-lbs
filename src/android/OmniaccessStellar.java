package info.survite.lbs.plugin;


import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;

import org.apache.cordova.*;
import org.json.JSONArray;
import org.json.JSONException;
import android.util.Log;
import android.content.Context;
import android.location.Location;
import android.content.DialogInterface;

import com.polestar.naosdk.api.external.NAOERRORCODE;
import com.polestar.naosdk.api.external.NAOGeofencingHandle;
import com.polestar.naosdk.api.external.NAOGeofencingListener;
import com.polestar.naosdk.api.external.NAOLocationHandle;
import com.polestar.naosdk.api.external.NAOLocationListener;
import com.polestar.naosdk.api.external.NAOSensorsListener;
import com.polestar.naosdk.api.external.NAOServicesConfig;
import com.polestar.naosdk.api.external.NAOSyncListener;
import com.polestar.naosdk.api.external.NaoAlert;
import com.polestar.naosdk.api.external.TNAOFIXSTATUS;

public class OmniaccessStellar extends CordovaPlugin implements NAOLocationListener,NAOSensorsListener,NAOGeofencingListener {
    private NAOGeofencingHandle geofencingHandle;
    private NAOLocationHandle locationHandle;
    private CallbackContext locationCallbackContext;
    private Location currentLocation;
    @Override
    public boolean execute(String action, JSONArray data, CallbackContext callbackContext) throws JSONException {

        if (action.equals("init")) {
            Context context = this.cordova.getActivity().getApplicationContext();

            String api_key = data.getString(0);

            locationHandle = new NAOLocationHandle(context, LbsService.class, api_key, this, this);

            geofencingHandle = new NAOGeofencingHandle(context, LbsService.class, api_key, this, this );

            downloadNAOResources();

            callbackContext.success("LBS-plugins : init OK");

            checkPermissionsAndStartLocation();

            return true;
        } else if (action.equals("location")) {
          Log.e("Init location handler", "");
          this.locationCallbackContext = callbackContext;
          if(this.currentLocation != null) {
            this.locationCallbackContext.success("{\"latitude\": " + currentLocation.getLatitude()
            + ", \"longitude\":" + currentLocation.getLongitude()
            + ", \"altitude\":" + currentLocation.getAltitude() + " }");
          }
          return true;
      } else {

            return false;

        }
    }

    public void downloadNAOResources() {
        locationHandle.synchronizeData(new NAOSyncListener() {
            @Override
            public void onSynchronizationSuccess() {
                Log.d("LBS-plugins", "Synchronization Success");
            }

            @Override
            public void onSynchronizationFailure(NAOERRORCODE errorCode, String message) {
                Log.e("LBS-plugins", "Synchronization Failure: " + message);
            }
        });
    }

    public void checkPermissionsAndStartLocation() {
        DialogInterface.OnClickListener permissionDialog = new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onPermissionsRefused();
            }
        };
        if (PermissionsUtils.checkAndRequest(this.cordova.getActivity(), NAOServicesConfig.BASIC_PERMISSIONS, "hello world", 0, permissionDialog)){
            onPermissionsGranted();
        }
    }

    private void onPermissionsGranted(){
        Log.d("LBS-plugins", "Location starts...");
        //we need ACCESS_FINE_LOCATION permission to use on-site wake-up
        //activateOnSiteWakeUp();
        locationHandle.start();
        geofencingHandle.start();
    }

    private void onPermissionsRefused(){
        Log.e("LBS-plugins", "Cannot run the service because permissions have been denied");
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            //locationText.setText(location.toString());
            try {
              Log.e("LBS-plugins", location.toString());
            } catch (Exception e) {
              Log.e("LBS-plugins", e.toString());
            }
            if(this.locationCallbackContext != null) {
              Log.e("Send location to callback","");
              this.currentLocation = location;

              this.locationCallbackContext.success("{\"latitude\": " + currentLocation.getLatitude()
                + ", \"longitude\":" + currentLocation.getLongitude()
                + ", \"altitude\":" + currentLocation.getAltitude() + " }");
            } else {
              this.currentLocation = location;
            }
        }
    }

    @Override
    public void onLocationStatusChanged(TNAOFIXSTATUS tnaofixstatus) {
      Log.e("LBS-plugins onLocationStatusChanged", "location status changed");

    }

    @Override
    public void onEnterSite(String s) {
      Log.e("LBS-plugins EnterSite:", s);

    }

    @Override
    public void onExitSite(String s) {
      Log.e("LBS-plugins ExitSite:", s);

    }

    @Override
    public void onError(NAOERRORCODE naoerrorcode, String s) {
      Log.e("LBS-plugins Error:", naoerrorcode + " : " + s);

    }

    @Override
    public void requiresCompassCalibration() {

    }

    @Override
    public void requiresWifiOn() {

    }

    @Override
    public void requiresBLEOn() {

    }

    @Override
    public void requiresLocationOn() {

    }

    @Override
    public void onFireNaoAlert(NaoAlert naoAlert) {
        if (naoAlert != null) {
          Log.e("LBS-plugins", naoAlert.toString());
            //Toast.makeText(getApplicationContext(), naoAlert.toString(), Toast.LENGTH_LONG).show();
        }
    }
}
